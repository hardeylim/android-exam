package viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import model.Person

class PersonViewModel : ViewModel() {
    var persons : MutableLiveData<ArrayList<Person>> = MutableLiveData()
    var person : MutableLiveData<Person> = MutableLiveData()
    var db = FirebaseFirestore.getInstance()

    fun getPersons() : LiveData<ArrayList<Person>> {
        if(persons.value == null){
            db.collection("persons")
                    .get()
                    .addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            var convertedPersons  = ArrayList<Person>()
                            for (document in task.result){
                                convertedPersons.add(document.toObject(Person::class.java))
                                persons.postValue(convertedPersons)
                            }
                        }else{

                        }
                    }


        }

        return persons
    }


    fun getPerson(docId: String = "") : LiveData<Person>{
        var docRef = db.collection("persons")
                                        .document(docId)

        docRef.addSnapshotListener{ documentSnapshot, e ->
            if(documentSnapshot!=null){
                var updatedPerson = documentSnapshot.toObject(Person::class.java)
                person.postValue(updatedPerson)
            }else throw Exception(e!!.message)
        }

        return person
    }

}