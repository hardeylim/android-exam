package utility.firebase;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

// No tests. Unable to mock firebase classes.
class DefaultFcmTokenProvider implements FcmTokenProvider {

    private FirebaseInstanceId firebaseInstanceId;

    @Inject
    DefaultFcmTokenProvider(FirebaseInstanceId firebaseInstanceId) {
        this.firebaseInstanceId = firebaseInstanceId;
    }

    @Override
    public String getFcmToken() {
        return firebaseInstanceId.getToken();
    }
}
