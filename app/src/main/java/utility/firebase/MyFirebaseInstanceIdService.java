package utility.firebase;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Inject
    FirebaseFirestore firebaseFirestore;

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    public void onTokenRefresh() {
//        String refreshedToken = FirebaseInstanceId.getInstance()
//                                                  .getToken();
//
//        firebaseFirestore.collection("devices")
//                         .document(getSerialNumber(this))
//                         .update("fcmToken", refreshedToken);
    }
}
