package utility.firebase;

public interface FcmTokenProvider {

    String getFcmToken();
}
