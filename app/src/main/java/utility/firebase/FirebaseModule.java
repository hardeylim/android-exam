package utility.firebase;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FirebaseModule {

    @ContributesAndroidInjector
    abstract MyFirebaseInstanceIdService bindMyFirebaseInstanceIdService();

    @Provides
    @Singleton
    static FirebaseFirestore provideFirebaseFirestore() {
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings firebaseFirestoreSettings =
            new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .setPersistenceEnabled(true)
                .build();
        database.setFirestoreSettings(firebaseFirestoreSettings);

        return database;
    }

    @Provides
    @Singleton
    static FcmTokenProvider fcmTokenProvider() {
        return new DefaultFcmTokenProvider(FirebaseInstanceId.getInstance());
    }

}
