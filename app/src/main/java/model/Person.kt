package model

data class Person (val id : String = "",
                   val firstName : String = "",
                   val lastName : String= "",
                   val birthday : String = "",
                   val age : Int = 0,
                   val emailAddress : String = "",
                   val mobileNumber : String = "",
                   val address : String = "",
                   val contactPerson : String = "",
                   val contactPersonNumber : String = ""){
}