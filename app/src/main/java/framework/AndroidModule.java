package framework;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Module that provides Android Framework related dependencies.
 */
@Module
public class AndroidModule {

    @Provides
    Context provideContext(ExamApplication application) {
        return application.getApplicationContext();
    }
}
