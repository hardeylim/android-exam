package framework;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import com.example.hardeylim.androidexam.BuildConfig;
import com.squareup.leakcanary.LeakCanary;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import io.reactivex.internal.functions.Functions;
import io.reactivex.plugins.RxJavaPlugins;
import timber.log.Timber;

public class ExamApplication extends Application
    implements HasActivityInjector, HasServiceInjector {

    private ExamComponent examComponent;

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidActivityInjector;

    @Inject
    DispatchingAndroidInjector<Service> dispatchingAndroidServiceInjector;


    @Override
    public void onCreate() {
        super.onCreate();
        initializeComponent().inject(this);

        if (BuildConfig.DEBUG) {
            initializeTimber();
            initializeLeakCanary();
        }

        // in the event that we have streams where the errors are delayed until all upstreams are
        // finished, RxJava will crash the app stating that some errors were not handled by the
        // subscriber properly. this is an intended behaviour - we don't want to have to process
        // those errors so we'll just provide an "Empty Consumer" for these errors
        RxJavaPlugins.setErrorHandler(Functions.emptyConsumer());

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return null;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return null;
    }

    ExamComponent initializeComponent() {
        examComponent = DaggerExamComponent.builder()
                                           .application(this)
                                           .build();
        return examComponent;
    }

    private void initializeLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this))
            LeakCanary.install(this);
    }

    private void initializeTimber() {
        Timber.plant(new Timber.DebugTree());
    }
}
