package framework;


import com.example.hardeylim.androidexam.screen.ScreenModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import utility.firebase.FirebaseModule;

@Component(
    modules = {
        FirebaseModule.class,
        ScreenModule.class

    })

@Singleton
public interface ExamComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(ExamApplication examApplication);

        ExamComponent build();
    }

    void inject(ExamApplication examApplication);
}
