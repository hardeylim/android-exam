package com.example.hardeylim.androidexam.screen

import dagger.Module
import dagger.android.ContributesAndroidInjector
import framework.ActivityScope


@Module
public interface ScreenModule {


    @ActivityScope
    @ContributesAndroidInjector
    fun personActivity() : PersonActivity

}