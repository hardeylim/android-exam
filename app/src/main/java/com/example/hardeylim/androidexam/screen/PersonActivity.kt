package com.example.hardeylim.androidexam.screen

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.hardeylim.androidexam.R
import com.example.hardeylim.androidexam.databinding.ActivityPersonBinding
import viewmodel.PersonViewModel

class PersonActivity() : AppCompatActivity() {

    val personId: String = "b2O7e2oAQ0WkDFrOXGdC"
    lateinit var binding: ActivityPersonBinding
    lateinit var viewModel: PersonViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBinding()

    }

    override fun onResume() {
        super.onResume()

        viewModel.getPerson(personId)
                .observeForever { person ->
                    println(message = person!!.birthday)
                    binding.person = person
                }


    }

    private fun initDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_person)
        this.viewModel = ViewModelProviders.of(this)
                .get(PersonViewModel::class.java)

    }


}