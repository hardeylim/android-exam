package com.example.hardeylim.androidexam.screen

import android.support.v7.widget.RecyclerView
import com.example.hardeylim.androidexam.databinding.ItemPersonListBinding

class PersonViewHolder(val binding: ItemPersonListBinding?) : RecyclerView.ViewHolder(binding!!.root)