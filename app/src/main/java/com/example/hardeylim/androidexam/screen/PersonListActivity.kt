package com.example.hardeylim.androidexam.screen

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.hardeylim.androidexam.R
import model.Person
import viewmodel.PersonViewModel
import java.util.ArrayList

class PersonListActivity : AppCompatActivity() {
    lateinit var  recyclerView : RecyclerView
    lateinit var  personAdapter: PersonAdapter
    lateinit var viewModel: PersonViewModel
    var  persons = ArrayList<Person>()

     override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_person_list)
         recyclerView = this.findViewById(R.id.person_recycler)

         initializePersons()
         showRecycler()
    }

    private fun initializePersons(){
        var person = Person(firstName = "Test firstName", lastName = "lastName")
        var person2 = Person(firstName = "Test firstName2")
        var person3 = Person(firstName = "Test firstName3")
        var person4 = Person(firstName = "Test firstName4")
        var person5 = Person(firstName = "Test firstName5")
        var person6 = Person(firstName = "Test firstName6")
        var person7 = Person(firstName = "Test firstName7")
        var person8 = Person(firstName = "Test firstName8")
        var person9 = Person(firstName = "Test firstName9")
        var person10 = Person(firstName = "Test firstName10")
        var person11 = Person(firstName = "Test firstName11")
        var person12 = Person(firstName = "Test firstName12")
        var person13 = Person(firstName = "Test firstName13")
        var person14 = Person(firstName = "Test firstName14")
        var person15 = Person(firstName = "Test firstName15")
        persons.add(person)
        persons.add(person2)
        persons.add(person3)
        persons.add(person4)
        persons.add(person5)
        persons.add(person6)
        persons.add(person7)
        persons.add(person8)
        persons.add(person9)
        persons.add(person10)
        persons.add(person11)
        persons.add(person12)
        persons.add(person13)
        persons.add(person14)
        persons.add(person15)
    }

//    private fun observePersons(){
//        viewModel.getPersons()
//                .observeForever{
//
//                }
//
//    }

    private fun showRecycler(){
        personAdapter = PersonAdapter(persons)
        recyclerView.adapter = personAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }
}