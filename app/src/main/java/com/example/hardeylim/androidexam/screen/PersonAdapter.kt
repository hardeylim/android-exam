package com.example.hardeylim.androidexam.screen

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.hardeylim.androidexam.R
import com.example.hardeylim.androidexam.databinding.ItemPersonListBinding
import model.Person

class PersonAdapter(var persons: List<Person>) : RecyclerView.Adapter<PersonViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup?, viewType: Int): PersonViewHolder {
        var context = viewGroup!!.context
        var layoutIdForListItem = R.layout.item_person_list
        var inflater = LayoutInflater.from(context)
        var shouldAttachToParentImmediately = false

        var view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)

        var binding = DataBindingUtil.inflate<ItemPersonListBinding>(
                inflater,
                layoutIdForListItem,
                viewGroup,
                shouldAttachToParentImmediately
        )
        return PersonViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PersonViewHolder?, position: Int) {
        holder!!.binding!!.person = persons[position]
    }

    override fun getItemCount(): Int {
        return persons.size
    }


}