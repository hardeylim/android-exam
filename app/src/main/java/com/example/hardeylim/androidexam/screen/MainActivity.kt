package com.example.hardeylim.androidexam.screen

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.hardeylim.androidexam.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showPersonActivity()
    }

    private fun showPersonActivity(){
        val handler = Handler()
        handler.postDelayed({
            finish()
            startActivity(Intent(this, PersonListActivity::class.java))
        }, 1000)
    }
}
